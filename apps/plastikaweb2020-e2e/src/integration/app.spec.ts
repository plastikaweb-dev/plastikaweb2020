import { getGreeting } from '../support/app.po';

describe('plastikaweb2020', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to plastikaweb2020!');
  });
});
