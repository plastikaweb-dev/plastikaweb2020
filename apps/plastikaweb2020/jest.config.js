module.exports = {
  name: 'plastikaweb2020',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/plastikaweb2020',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
